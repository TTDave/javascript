//
// 
// You ONLY need function on and off for the 
// overlay form to work 
//
//
//
//
//
$(document).ready(
  function() {
    // detta händer då man skriver i filter-boxen. bl.a. ESC tömmer boxen
    $("#filter").keyup(function(event) {
      //if esc is pressed or nothing is entered
      if (event.keyCode == 27 || $(this).val() == "") {
        //if esc is pressed we want to clear the value of search box
        empty_filterbox();
      } else {
        filter2("#mysql tbody tr", $(this).val());
      }

    });

    // om någon trycker ESC över huvudtaget, så töm search och visa allt
    $("body").keyup(function(e) {
      if (e.keyCode == 27) {
        var overlay = document.getElementById("overlay");
        // If form style is none it emptys filter box othervise it closes the form
        if (overlay.style.display === "none") {
          empty_filterbox();
        } else {
          off();
        }
      }
      if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode == 0)) {
        var test = document.getElementById("overlay");
        // If any of the keys a-z 0-9 or åäö is pressed and the form is closed it will focus on the search othervise it will do nothing
        if (test.style.display === "none") {
          document.getElementById("filter").focus();
        }
      }
    });
  });

//
// 
// You ONLY need function on and off for the 
// overlay form to work
//
//
//
//
//
function on() {
  document.getElementById("overlay").style.display = "block";
}

// closes the form and clears it
function off() {
  document.getElementById("overlay").style.display = "none";
  $("#id").val("");
  $("#email").val("");
  $("#domains").val("");
  $("#rights").val("");
  $("#datepicker").val("");
  $("#name").val("");
}

// 
// you only need ^ those two
//
//
//
//
//
//
//
// Tests if header is top is off screen or not each time you scroll
window.onscroll = function() {
  content();
};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;
// if the header is out of the screen it will become "sticky" and stick on the top of the screen fixed
function content() {
  var top = document.getElementById("topContent");
  if (window.pageYOffset >= sticky) {
    header.classList.add("sticky");
    top.classList.add("scroll");
  } else {
    header.classList.remove("sticky");
    top.classList.remove("scroll");

  }
}

$(function() {
  $("#datepicker").datepicker({
    dateFormat: "yy-mm-dd"
  });
});


//OLD CLEAR FORM FUNC
// function func(id) {
//   $(document).ready(function() {
//     $("#id"+id).val("");
//     $("#email"+id).val("");
//     $("#domains"+id).val("");
//     $("#rights"+id).val("");
//     $("#datepicker"+id).val("");
//     $("#name"+id).val("");
//   });
// }

function submit() {
  document.getElementById("form_id").submit();
}

function filter2(selector, query, showselected) {

  query = $.trim(query); //trim white space
  query = query.replace(/ /gi, "|"); //add OR for regex query

  $(selector).each(function() {
    if ($(this).text().search(new RegExp(query, "i")) < 0) {
      $(this).hide().removeClass("visible");
    } else {
      $(this).show().addClass("visible");
    }
  });
}

function empty_filterbox(filterboxname, tablename) {
  // Sätt ett default filterboxname
  if (undefined === filterboxname) {
    filterboxname = "filter";
  }

  // Töm
  $("#" + filterboxname).val("");
  $("#" + filterboxname).focus();

  // Gör nåt vettigt om tablename inte är definerat
  if (undefined === tablename) {
    tablename = "";
  } else {
    tablename = "#" + tablename;
  }
  show_all_rows(tablename + " tbody tr");
}

// Visa alla rader
function show_all_rows(selector) {
  $(selector).removeClass("visible").show().addClass("visible");
}

// ORIGINAL FILTER FUNC
// function filter(n, id) {
//   // Declare variables
//   var input, filter, table, tr, td, i;
//   input = document.getElementById(id);
//   filter = input.value.toUpperCase();
//   table = document.getElementById("mysql");
//   tr = table.getElementsByTagName("tr");
//
//   // Loop through all table rows, and hide those who don"t match the search query
//   for (i = 0; i < tr.length; i++) {
//     td = tr[i].getElementsByTagName("td")[n];
//     if (td) {
//       if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
//         tr[i].style.display = "";
//       } else {
//         tr[i].style.display = "none";
//       }
//     }
//   }
// }

// function funcC(val1, val2, val3, val4, val5, val6) {
//   $(document).ready(function() {
//     $("#id").val(val1);
//     $("#email").val(val2);
//     $("#domains").val(val3);
//     $("#rights").val(val4);
//     $("#datepicker").val(val5);
//     $("#name").val(val6);
//   });
// }
//TRY MAKING THIS WORK ADD CLASS TO ROW  https://stackoverflow.com/questions/14460421/get-the-contents-of-a-table-row-with-a-button-click
// $(".use-address").click(function() {
//   var $row = $(this).closest("tr"), // Finds the closest row <tr>
//     $tds = $row.find("td"); // Finds all children <td> elements
//   var list = [];
//   var i = 0;
//   $.each($tds, function() { // Visits every single <td> element
//     list[i] = ($(this).text()); // Prints out the text within the <td>
//     i++;
//   });
//   console.log(list[0]);
//   console.log(list[1]);
//   console.log(list[2]);
//   console.log(list[3]);
//   console.log(list[4]);
//   console.log(list[5]);
//   $("#id").val(list[0]);
//   $("#email").val(list[1]);
//   $("#domains").val(list[2]);
//   $("#rights").val(list[3]);
//   $("#datepicker").val(list[4]);
//   $("#name").val(list[5]);
//   on();
// });

function click_row(id) {
  var $row = $("#row_" + id);
  $tds = $row.find("td"); // Finds all children <td> elements
  var list = [];
  var i = 0;
  $.each($tds, function() { // Visits every single <td> element
    list[i] = ($(this).text()); // Puts the text of each <td> element into a list named list
    i++;
  });
  //Sthe value of attribute id is sliced of its 4 first letter since we only want the number and not row_
  $("#id").val($row.attr("id").slice(4));
  $("#email").val(list[0]);
  $("#domains").val(list[1]);
  $("#rights").val(list[2]);
  $("#datepicker").val(list[3]);
  $("#name").val(list[4]);
  on("1");
}
